import logo from './logo.svg';
import './App.css';
import favicon from './favicon.png'

function App() {
    return (
        <div className="App" >
            <header className="App-header" >
                <img src={favicon}
                    className="App-logo"
                    alt="logo" />

               <h6>
                   Hello Kubernetes Updates Check
               </h6>
            </header>
        </div>
    );
}

export default App;